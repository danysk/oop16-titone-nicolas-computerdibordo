package streamline;

public class StopSelfMessage extends StreamLineSelfMessage {
	
	public StopSelfMessage(int streamLineId, Object value) {
		
		super(streamLineId, value);
	}
}
